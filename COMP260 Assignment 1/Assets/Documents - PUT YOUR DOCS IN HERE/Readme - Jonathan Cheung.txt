As I was finalising the project, I tried to sort the terrains in the asset folder into their own folders and 
somehow it accidentally removed the terrain assets from my scenes, so I had to re-implement them. 
I'm unsure if any issues will arise from this but I don't think there will be any, I should have 
set all the new terrain assets to terrain layer.

I haven't re-baked the navmesh after this occurred, so the zombies should be operating normally.

In case lighting settings are broken, it's set to a black Ambient Colour with a reflection intensity of
0.2, with skybox on.